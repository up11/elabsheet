from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.http import HttpResponseRedirect
from django.views.decorators.cache import never_cache
from django.contrib.auth.views import login as auth_views_login
from django.contrib.auth.models import User

from logger.models import Log

def login(request, template_name='registration/login.html', redirect_field_name=REDIRECT_FIELD_NAME):
    if settings.HTTPS_LOGIN and (not request.is_secure()):
        new_url = "https://%s%s" % (request.get_host(), request.get_full_path())
        return HttpResponseRedirect(new_url)
    else:
        if request.method=='GET':
            return auth_views_login(request, template_name, redirect_field_name)
        else:
            result = auth_views_login(request, template_name, redirect_field_name)
            if isinstance(result,HttpResponseRedirect):
                # login successful
                result['Location'] = 'http://%s%s' % (request.get_host(), result['Location'])
                
                user = User.objects.get(username=request.POST['username'])
                Log.create("login", request, user=user)
            else:
                # login failed
                Log.create("failed-login", request,
                           comment=("user: %s" % request.POST['username']))
                
            return result

login = never_cache(login)


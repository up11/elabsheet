from django.contrib import admin
from django import forms
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe

from .models import \
        Semester, Section, LabInSection, \
        AddressAcl, SingleIpLock, DirectToLabAccount

admin.site.register(Semester)

class LabInSectionInline(admin.TabularInline):
    model = LabInSection
    fields = ['number', 'lab']
    autocomplete_fields = ['lab']
    verbose_name = 'Lab in this section'
    verbose_name_plural = 'Labs in this section'
    extra = 3


class SectionAdminForm(forms.ModelForm):
    class Meta:
        model = Section
        fields = ['course', 'semester', 'name', 'notes', 'instructors']

    instructors = forms.ModelMultipleChoiceField(
        queryset=User.objects.filter(is_staff=True),
        widget=admin.widgets.FilteredSelectMultiple("staffs",False))


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    inlines = [LabInSectionInline]
    form = SectionAdminForm
    list_display = ('course', 'semester', 'name', 'notes')
    search_fields = ['course__name', 'course__number', 'name']


@admin.register(LabInSection)
class LabInSectionAdmin(admin.ModelAdmin):
    fields = ['hidden_tasks']


@admin.register(AddressAcl)
class AddressAclAdmin(admin.ModelAdmin):
    fields = [
        'labinsec',
        'activated',
        'allowed_list',
        'processed_allowed_list',
        'denied_list',
        'processed_denied_list',
        'default_action',
        'single_ip',
    ]
    raw_id_fields = ['labinsec']
    readonly_fields = ['processed_allowed_list', 'processed_denied_list']
    list_display = ['labinsec','default_action','activated']

    def processed_allowed_list(self, instance):
        return mark_safe("<tt>" + str(instance.get_allowed_list()) + "</tt>")

    def processed_denied_list(self, instance):
        return mark_safe("<tt>" + str(instance.get_denied_list()) + "</tt>")


@admin.register(SingleIpLock)
class SingleIpLockAdmin(admin.ModelAdmin):
    raw_id_fields = ['user','labinsec']
    list_display = ['user','labinsec','ip','last_access']


@admin.register(DirectToLabAccount)
class DirectToLabAccountAdmin(admin.ModelAdmin):
    fields = ['username','password','enabled','user','labinsec']
    raw_id_fields = ['user','labinsec']
    list_display = ['labinsec','user','username','password','enabled']

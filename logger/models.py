from django.db import models
from django.contrib.auth.models import User, AnonymousUser
from django.utils import timezone
from commons.utils import get_remote_addr_from_request

class Log(models.Model):
    user = models.ForeignKey(User,null=True,blank=True,on_delete=models.SET_NULL)
    request_ip = models.GenericIPAddressField(blank=True,null=True)
    event = models.CharField(max_length=50,db_index=True)
    comment = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True,db_index=True)

    def __str__(self):
        if self.comment=="":
            return "%s: [%s] (%s/%s)" % (self.event.upper(), 
                                         timezone.localtime(self.created_at),
                                         self.user,
                                         self.request_ip)
        else:
            return "%s: %s [%s] (%s/%s)" % (self.event.upper(), 
                                            self.comment,
                                            timezone.localtime(self.created_at),
                                            self.user,
                                            self.request_ip)

    class Meta:
        ordering = ['-created_at']

    @staticmethod
    def create(event,request=None,comment="",user=None,request_ip=None):
        if request!=None:
            if user==None:
                if not isinstance(request.user, AnonymousUser):
                    user = request.user
            if request_ip==None:
                request_ip = get_remote_addr_from_request(request)

        log = Log(user=user, 
                  request_ip=request_ip,
                  event=event, 
                  comment=comment)
        log.save()

